package com.yunfinal.api.demo;

import com.yunfinal.api.service.doc.ApiDataHttp;
import com.yunfinal.api.service.doc.ApiDocData;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestDemoApi {
    ApiDataHttp http = ApiDataHttp.ME;
    String base = "/api";


    @Test
    public void index() {
        ApiDocData data = http.newApiDocData();
        http.doc(base + "/index", data);
    }

    @Test
    public void json() {
        ApiDocData data = http.newApiDocData();
        data.putMust("teacherName", "詹老师", "老师名称");

        List<ApiDocData> projects = new ArrayList<>();
        data.put("projects", projects, "软件作品");

        ApiDocData project_1 = http.newApiDocData();
        projects.add(project_1);
        project_1.put("name", "JFinal", "软件名称");
        project_1.put("synopsis", "JAVA 极速WEB+ORM框架", "摘要");
        ApiDocData project_1_data = http.newApiDocData();
        project_1.put("data", project_1_data, "历史");
        project_1_data.put("2012", "入住开源中国", "年份=内容");
        project_1_data.put("2013", "中国最受欢迎软件N1");
        project_1_data.put("2014", "中国最受欢迎软件N1");
        project_1_data.put("2015", "中国最受欢迎软件N1");
        project_1_data.put("2016", "中国最受欢迎软件N1");
        project_1_data.put("2017", "中国最受欢迎软件N1");
        project_1_data.put("2018", "20W+ 开发者");
        project_1_data.put("2019", "中国自由创业联盟");

        ApiDocData project_2 = http.newApiDocData();
        projects.add(project_2);
        project_2.put("name", "JFinal Weixin");
        project_2.put("synopsis", "微信公众号极速 SDK");

        ApiDocData project_3 = http.newApiDocData();
        projects.add(project_3);
        project_3.put("name", "Enjoy");
        project_3.put("synopsis", "Java 极轻量级模板引擎");

        ApiDocData project_4 = http.newApiDocData();
        projects.add(project_4);
        project_4.put("name", "JFinal Undertow");
        project_4.put("synopsis", "JFinal 项目部署环境");

        //接口文档 生成
        http.docGenerate(base + "/json", data);
    }

    @Test
    public void error() {
        ApiDocData data = http.newApiDocData();
        //data.putMust("name", "value", "名称");
        http.doc(base + "/error", data);
    }

    @Test
    public void toRecord() {
        ApiDocData data = http.newApiDocData();
        data.put("name", "value", "名称");
        http.doc(base + "/toRecord", data);
    }

    @Test
    public void exception() {
        ApiDocData data = http.newApiDocData();
        http.doc(base + "/exception", data);
    }

    @Test
    public void db() {
        ApiDocData data = http.newApiDocData();
        data.putMust("token", "1234567", "令牌");
        http.doc(base + "/db", data);
    }

}
