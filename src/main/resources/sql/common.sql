#define addPara(val)
  #if(val)
    #if(CondKvKit.isArray(val))
      #(CondKvKit.addArray(_SQL_PARA_, val))
    #else
      #para(val)
    #end
  #end
#end

#sql("find")
SELECT #(select??"*")
     FROM #(table)
  #for(x : cond)
     #(for.index == 0 ? "WHERE" : "AND") #(x.key) #@addPara(x.value)
  #end

  #if(groupBy)
     GROUP BY #(groupBy)
  #end
  #if(orderBy)
     ORDER BY #(orderBy)
  #end
  #if(limit)
     LIMIT #(limit.pageSize * (limit.pageNumber - 1)), #(limit.pageSize)
  #end
#end
