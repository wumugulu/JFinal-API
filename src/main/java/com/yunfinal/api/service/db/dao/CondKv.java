package com.yunfinal.api.service.db.dao;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;

/**
 * CondKv 是cond条件参数的一个封装类， 方便使用
 *
 * @author 王双菊
 */
public class CondKv extends Kv {

    public static CondKv by() {
        return new CondKv();
    }

    public static CondKv by(Object key, Object value) {
        return new CondKv().set(key, value);
    }

    @Override
    public CondKv set(Object key, Object value) {
        super.set(key, value);
        return this;
    }

    /**
     * 无占位符写法，
     *
     * @param key
     * @param values
     * @return
     */
    public CondKv setSql(String key, Object... values) {
        return set(key, values);
    }

    /**
     * 判断kv中的kvKey值value不为空，则增加set
     *
     * @param condKey
     * @param kv
     * @param kvKey
     * @return
     */
    public CondKv setNotBlank(String condKey, JSONObject kv, String kvKey) {
        String value = kv.getString(kvKey);
        return StrKit.isBlank(value) ? this : set(condKey, value);
    }

    /**
     * 拼接时间
     *
     * @param data
     * @return
     */
    public CondKv setDateTimeStr(String condKey, String data) {
        if (StrKit.isBlank(data)) {
            return this;
        }
        return set(condKey, data.length() > 10 ? data : data.concat(" 00:00:00"));
    }
}
