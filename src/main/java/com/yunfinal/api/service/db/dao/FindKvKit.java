package com.yunfinal.api.service.db.dao;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * 方便用于像连表查询等独立查询
 */
public class FindKvKit {

    public static List<Record> find(FindKv kv) {
        List<Record> ret = Db.find(Db.getSqlPara("find", kv));
        return ret;
    }

    public static Record findFirst(FindKv kv) {
        kv.setLimit(1);
        Record ret = Db.findFirst(Db.getSqlPara("find", kv));
        return ret;
    }
}
